<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('workers', function (Blueprint $table) {
//            $table->increments('id');
            $table->string('id');
            $table->string('email')->unique();
            $table->string('password');
            $table->string('assignment');
            $table->string('location');
            $table->string('first_name');
            $table->string('surname');
            $table->string('other_names');
            $table->string('phone');
            $table->longText('image')->nullable();
            $table->string('api_token')->nullable();
            $table->rememberToken();
            $table->timestamp('modified')->nullable();
            $table->timestamp('created')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('workers');
    }
}
