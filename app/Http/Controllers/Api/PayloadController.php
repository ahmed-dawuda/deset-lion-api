<?php

namespace App\Http\Controllers\Api;

use App\Models\Item;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class PayloadController extends Controller
{
    public function submit_payload (Request $request)
    {
        $validator = Validator::make($request->all(), [
            'payload'=> 'required',
            'access' => 'required'
        ]);


        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }

        $data = $request->payload;

        foreach ($data as $table => $rows) {
            foreach ($rows as $row) {
                $entry = DB::table($table)->where('id', $row['id'])->first();
                if (!$entry) {
                    DB::table($table)->insert($row);
                } else {
                    DB::table($table)->where('id', $row['id'])->update($row);
                }
            }
        }

        return response()->json(['message' => 'Payload saved'], 200);
    }
    
    public function request_payload (Request $request)
    {
        $validator = Validator::make($request->all(), [
            'access' => 'required',
            'last_served' => 'numeric'
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }

        $last_served = $request->last_served;

        if(!$last_served) {
            $last_served = 0;
        }

        $payload = null;

//        return $request->all();

        switch ($request->access) {
            case 'Management':
                $payload = $this->prepare_management_payload($last_served);
                break;
            case 'Field':
                $payload = $this->prepare_field_payload($last_served);
                break;
            case 'Warehouse':
                $payload = $this->prepare_warehouse_payload($last_served);
                break;
            default:
                $payload = ['status'=> false, 'message'=> []];
        }

        return $payload;
    }

    public function prepare_management_payload($last_served){
        $tables = ['waybills', 'inventory', 'supplies'];
        $payload = $this->get_payload($last_served, $tables);
        $ids = [];
        foreach ($payload as $table => $rows) {
            foreach ($rows as $row) {
                $ids[] = $row->id;
            }
        }

        $payload['goods'] = DB::table('items')->where('modified', '>=', $last_served)->whereIn('record', $ids)->get();
        return ['message'=> $payload, 'status'=> true];
    }

    public function prepare_field_payload($last_served){
        $tables = ['waybills', 'products'];
        $payload = $this->get_payload($last_served, $tables);
        $ids = [];
        foreach ($payload['waybills'] as $waybill) {
            $ids[] = $waybill->id;
        }

        $payload['goods'] = DB::table('items')->where('modified', '>=', $last_served)->whereIn('record', $ids)->get();
        return ['message' => $payload, 'status'=> true];
    }

    public function prepare_warehouse_payload($last_served){
        $tables = ['zones', 'products', 'directives'];
        $payload = $this->get_payload($last_served, $tables);
        $directive_ids = [];
        foreach ($payload['directives'] as $directive) {
            $directive_ids[] = $directive->id;
        }
        $payload['goods'] = DB::table('items')->where('modified', '>=', $last_served)->whereIn('record', $directive_ids)->get();
        return ['message'=> $payload, 'status'=> true];
    }

    public function get_payload($last_served, $tables = []) {
        $payload = [];
        foreach ($tables as $tablename) {
            $payload[$tablename == 'zones' ? 'recipients' : $tablename] = DB::table($tablename)->where('modified', '>=', $last_served)->get();
        }

        return $payload;
    }
}
