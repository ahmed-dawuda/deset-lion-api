<?php

namespace App\Http\Controllers\Api;

use App\Models\Assignment;
use Illuminate\Foundation\Auth\User;
use Illuminate\Support\Facades\Auth;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        //validate request data
        $validator = Validator::make($request->all(), [
            'email' => 'required',
            'password' => 'required',
            'access' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }

        $credentials = ['email'=> $request->email, 'password' => $request->password];

        //log user in
        if (Auth::attempt($credentials)) {
            $user = Auth::user();
            $assignment = Assignment::find($user->assignment);

            //check for access
            if ($assignment && $assignment->access == $request->access) {
                $token = bin2hex(random_bytes(20));
                $user->api_token = $token;
                $user->save();
                return response()->json(['user'=> $user], 200);
            } else {
                return response()->json(['error'=> "You're not authorized to use this application"], 401);
            }
        } else {
            return response()->json(['error'=> 'Auth Failed'], 401);
        }
    }

    public function logout (Request $request) {
        $user = Auth::user();
        $user->api_token = '';
        $user->save();
        return response()->json(['message' => 'ok'], 200);
    }
}
