<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Directive extends Model
{
    public $incrementing = false;
    protected $primaryKey = 'id';
    public $timestamps = false;
}
