<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Inventory extends Model
{
    public $incrementing = false;
    public $timestamps = false;
    public $table = 'inventory';
}
