<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Supply extends Model
{
    protected $primaryKey = 'id';
    public $incrementing = false;
    public $timestamps = false;
}
