<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/login', 'Api\AuthController@login');
Route::get('/logout', 'Api\AuthController@logout')->middleware('auth:api');
Route::post('/payload/submit', 'Api\PayloadController@submit_payload')->middleware('auth:api');
Route::get('/payload/request', 'APi\PayloadController@request_payload')->middleware('auth:api');
